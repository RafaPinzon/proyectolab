# -*- coding: utf-8 -*-


from django.forms import ModelForm

from usuarios.models.BasicosModel import SolicitudTratamiento

class SolicitudFormularioT(ModelForm):

    def clean(self):
        form_data = self.cleaned_data
        return form_data

    class Meta:
        model   = SolicitudTratamiento
        fields = ['nombre', 'descripcion']
