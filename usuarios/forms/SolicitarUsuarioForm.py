# -*- coding: utf-8 -*-


from django.forms import ModelForm

from usuarios.models.UsuariosModel import SolicitudesUsuarios

class SolicitudFormulario(ModelForm):

    def clean(self):
        form_data = self.cleaned_data
        if not form_data['es_paciente'] and not form_data['es_medico_general'] and not form_data['es_medico_especialista']:
            self._errors["es_paciente"] = "Debe seleccionar al menos un perfil"
            self._errors["es_medico_general"] = "Debe seleccionar al menos un perfil"
            self._errors["es_medico_especialista"] = "Debe seleccionar al menos un perfil"
        return form_data

    class Meta:
        model   = SolicitudesUsuarios
        fields = ['codigo', 'nombre1', 'nombre2', 'apellido1', 'apellido2', 'email', 'telefono', 'fechanacimiento', 'es_paciente', 'es_medico_general', 'es_medico_especialista']
