from django.views.generic import ListView
from ..models.BasicosModel import Medicamento, Tratamiento, GruposDeApoyo, EPS, Leyes, Enfermedad, Servicio, SolicitudMedicamento, SolicitudTratamiento
from ..models.UsuariosModel import Usuario, SolicitudesUsuarios
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import user_passes_test
from django.conf import settings
from django.db.models import Q



class Medicamento_list(ListView):
    model= Medicamento

class Leyes_list(ListView):
    model= Leyes

class Tratamientos_list(ListView):
    model= Tratamiento


class GruposDeApoyo_list(ListView):
    model= GruposDeApoyo


class Eps_list(ListView):
    model= EPS


class SolicitudesUsuarios_list(ListView):
    queryset  = SolicitudesUsuarios.objects.filter(estado=settings.ESTADO_ESPERA)
    model= SolicitudesUsuarios

class SolicitudMedicamento_list(ListView):
    model= SolicitudMedicamento

class SolicitudTratamiento_list(ListView):
    model= SolicitudTratamiento


class Pacientes_list(ListView):
    queryset  = SolicitudesUsuarios.objects.filter(es_paciente=True)
    model= Usuario

class MedicosG_list(ListView):
    queryset  = SolicitudesUsuarios.objects.filter(es_medico_general=True)
    model= Usuario

class MedicosE_list(ListView):
    queryset  = SolicitudesUsuarios.objects.filter(es_medico_especialista=True)
    model= Usuario

class Medicos_list(ListView):
    queryset  = SolicitudesUsuarios.objects.filter(Q(es_medico_general = True) | Q(es_medico_especialista = True))
    model= Usuario

