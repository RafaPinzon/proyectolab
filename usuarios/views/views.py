# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import user_passes_test
from ..models.UsuariosModel import Usuario
from django.contrib import messages

# Create your views here.
def index(request):
   return render_to_response('base2.html',context_instance= RequestContext(request))

@user_passes_test(lambda u: u.is_superuser,login_url = 'index')
def admin_medica(request):
	return render_to_response('Admin.html', context_instance= RequestContext(request))

def perfil(request):
    usuario = Usuario.objects.all()
    return render_to_response('user/Usuarios.html',{'usuarios': usuario},RequestContext(request))

def login_usuario(request):

	if not request.user.is_anonymous():
		return redirect('index')
	if request.method == 'POST':
		formulario= AuthenticationForm(request.POST)
		if formulario.is_valid:

			username= request.POST['username']
			clave= request.POST['password']
			acceso= authenticate(username= username, password= clave)
			if acceso is not None:
				if acceso.is_active:
					login( request, acceso)
					messages.info(request, "Bienvenido")
					return render_to_response('base3.html', {'username':username}, context_instance= RequestContext(request))
				else:
					messages.info(request, "Está inactivo")
					return redirect('index')#usuario inactivo
			else:
				messages.info(request, "Datos incorrectos")
	else:
		formulario=AuthenticationForm()

	return render_to_response('user/login.html', {'formulario':formulario}, context_instance= RequestContext(request))


@login_required(login_url= 'login')
def cerrar_sesion(request):
	logout(request)
	return redirect('index')
