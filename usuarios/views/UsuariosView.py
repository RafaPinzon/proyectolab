# -*- coding: utf-8 -*-

from ..forms.SolicitarUsuarioForm import SolicitudFormulario
from ..forms.SolicitarTratamientoForm import SolicitudFormularioT


from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from ..models.UsuariosModel import SolicitudesUsuarios, Usuario
from django.contrib import messages
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.mail import send_mail

def solicitar_usuario(request):
#metodos de envio de email defectuosos
    def send_accepted_registration_email():
        subject = 'su solicitud de registro Ha sido aceptada'
        message = 'su solicictud fue aceptada por el administrador, porfavor recuerde dar click a el link para terminar de llenar sus datos.(mirar perfil por el email )'
        from_email = settings.EMAIL_HOST_USER
        to_list = [settings.EMAIL_HOST_USER]
        #from_email = "blackzen666@gmail.com"
        #to_list = ["blackzen666@gmail.com"]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        print(from_email)
        print(to_list)
        return True

    def  send_denied_registration_email():
        subject = 'su solicitud de registro Ha sido denegada'
        message = 'su solicictud fue denegada por el administrador, porfavor revise los datos enviados en su solicitud para verificar si exite un error de lo cntrario contacte nuestra administracion link:.'
        from_email = settings.EMAIL_HOST_USER
        to_list = [settings.EMAIL_HOST_USER]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        print(from_email)
        print(to_list)
        return True

###debe hacerse en un scheduled task preguntar si es como ejemplo de poner a correr una cancion pero entonces como se hace con un script.
    def  send_denied_registration_email():
        subject = 'recordatorio toma de medicamentos y terapias de trtamientos.'
        message = 'recurde tomar su medicina o realizar su tratamiento (realizar consulta de tratamientos y medicamentos de usuario)'
        from_email = settings.EMAIL_HOST_USER
        to_list = [settings.EMAIL_HOST_USER]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        print(from_email)
        print(to_list)
        return True



    send_accepted_registration_email()

#############################################################################################################################
    if request.method == "POST":
        formulario = SolicitudFormulario(request.POST)
        if formulario.is_valid():
            formulario.save()
            return render_to_response(
                                        'usuarios/solicitud_enviada.html', {},
                                        context_instance = RequestContext(request)
                                        )
    else:
        formulario = SolicitudFormulario()

    return render_to_response(
                                'usuarios/solicitar.html',
                                {'formulario':formulario},
                                context_instance = RequestContext(request)
                                )

def aceptar_solicitud(request, solicitud_id):
    try:
        solicitud = SolicitudesUsuarios.objects.get(id = solicitud_id)
        usuario = Usuario.objects.create(
                    username = solicitud.codigo,
                    first_name = solicitud.nombre1 + " " + solicitud.nombre2,
                    last_name = solicitud.apellido1+ " " + solicitud.apellido2,
                    email = solicitud.email,
                    password = "temporal",
                    is_staff = False,
                    is_active = True,
                    is_superuser = False,
                    es_paciente = solicitud.es_paciente,
                    es_medico_general = solicitud.es_medico_general,
                    es_medico_especialista = solicitud.es_medico_especialista,
                    telefono = solicitud.telefono

            )
        password = Usuario.objects.make_random_password()
        usuario.set_password(solicitud.codigo)
        usuario.save()
        #Aquí tiene que enviarle correo con el password
        solicitud.estado = settings.ESTADO_ACEPTADO
        solicitud.save()

        if usuario.es_paciente:
            grupo = Group.objects.get(name = 'Pacientes')
            usuario.groups.add(grupo)

        if usuario.es_medico_general or usuario.es_medico_especialista:
            grupo = Group.objects.get(name = 'Médico')
            usuario.groups.add(grupo)

        messages.success(request, "Solicitud Aceptada")
    except:
        messages.error(request, "Error al aceptar solicitud")
    return redirect('solicitudes_usuarios')

def cancelar_solicitud(request, solicitud_id):
    try:
        solicitud = SolicitudesUsuarios.objects.get(id = solicitud_id)
        solicitud.estado = settings.ESTADO_RECHAZADO
        solicitud.save()
        messages.success(request, "Solicitud Rechazada")
    except:
        messages.error(request, "No se encontró la solicitud")

    return redirect('solicitudes_usuarios')

def eliminar_solicitud(request, solicitud_id):
    try:
        solicitud = SolicitudesUsuarios.objects.get(id = solicitud_id)
        solicitud.delete()
        messages.success(request, "Solicitud eliminada")
    except:
        messages.error(request, "No se encontró la solicitud")

    return redirect('solicitudes_usuarios')

def solicitar_tratamiento(request):
    if request.method == "POST":
        formulario = SolicitudFormularioT(request.POST)
        if formulario.is_valid():
            formulario.save()
            return render_to_response(
                                        'usuarios/solicitud_enviada.html', {},
                                        context_instance = RequestContext(request)
                                        )
    else:
        formulario = SolicitudFormularioT()

    return render_to_response(
                                'usuarios/solicitar_tratamiento.html',
                                {'formulario':formulario},
                                context_instance = RequestContext(request)
                                )
