# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0003_solicitudmedicamento_solicitudtratamiento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datosmedico',
            name='especialidad',
            field=models.ForeignKey(blank=True, to='usuarios.Especialidades', null=True),
            preserve_default=True,
        ),
    ]
