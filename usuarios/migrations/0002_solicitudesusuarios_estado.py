# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudesusuarios',
            name='estado',
            field=models.CharField(default=b'espera', max_length=20, choices=[(b'espera', b'En espera'), (b'aceptado', b'Aceptado'), (b'rechazado', b'Rechazado')]),
            preserve_default=True,
        ),
    ]
