# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0002_solicitudesusuarios_estado'),
    ]

    operations = [
        migrations.CreateModel(
            name='SolicitudMedicamento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=30)),
                ('descripcion', models.CharField(max_length=300)),
            ],
            options={
                'db_table': 'solicitud_medicamento',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SolicitudTratamiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=30)),
                ('descripcion', models.CharField(max_length=300)),
            ],
            options={
                'db_table': 'solicitud_tratamiento',
            },
            bases=(models.Model,),
        ),
    ]
