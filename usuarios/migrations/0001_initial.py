# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('es_paciente', models.BooleanField(default=False)),
                ('es_medico_general', models.BooleanField(default=False)),
                ('es_medico_especialista', models.BooleanField(default=False)),
                ('telefono', models.CharField(max_length=20, blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'permissions': (('solicitar_tratamiento', 'Puede solicitar tratamiento'), ('solicitar_medicamento', 'Puede solicitar medicamento'), ('listar_medicos', 'Puede listar medicos'), ('listar_pacientes', 'Puede listar pacientes'), ('editar_perfil', 'Puede editar perfil'), ('editar_perfil_paciente', 'Puede editar perfil paciente'), ('listar_grupos_de_apoyo', 'Puede listar grupos de apoyo'), ('agregar_a_grupo_apoyo', 'Puede agregar a grupo apoyo'), ('listar_servicios', 'Puede listar servicios'), ('listar_enfermedades', 'Puede listar enfermedades'), ('listar_quejas', 'Puede listar quejas'), ('crear_queja', 'Puede crear queja'), ('crear_receta', 'Puede crear receta'), ('listar_medicamentos', 'Puede listar medicamentos'), ('listar_tratamientos', 'Puede listar tratamientos'), ('listar_eps', 'Puede listar eps'), ('listar_leyes', 'Puede listar leyes'), ('asignar_paciente', 'Puede asignar paciente'), ('ver_paciente', 'Puede ver paciente')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DatosMedico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('foto', models.CharField(max_length=500, blank=True)),
                ('instituciones', models.CharField(max_length=300, blank=True)),
                ('tarifa', models.IntegerField(null=True, blank=True)),
                ('referencias', models.CharField(max_length=500, blank=True)),
            ],
            options={
                'db_table': 'medicoespecialista',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DatosPaciente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estrato', models.IntegerField(null=True, blank=True)),
                ('genero', models.CharField(max_length=40, blank=True)),
                ('edad', models.IntegerField(null=True, blank=True)),
                ('direcciondespacho', models.CharField(max_length=40, blank=True)),
                ('nombrecontacto', models.CharField(max_length=45, blank=True)),
                ('peso', models.IntegerField(null=True, blank=True)),
                ('altura', models.IntegerField(null=True, blank=True)),
                ('deportes', models.NullBooleanField()),
            ],
            options={
                'db_table': 'paciente',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Enfermedad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=25, blank=True)),
                ('sintomas', models.CharField(max_length=300, blank=True)),
                ('descripcion', models.CharField(max_length=300, blank=True)),
            ],
            options={
                'db_table': 'enfermedad',
                'verbose_name': 'Enfermedades',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EPS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40, blank=True)),
                ('direccion', models.CharField(max_length=55, blank=True)),
                ('telefono', models.CharField(max_length=20, blank=True)),
            ],
            options={
                'db_table': 'eps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Especialidades',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombreespecialidad', models.CharField(max_length=25, blank=True)),
            ],
            options={
                'db_table': 'especialidades',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GruposDeApoyo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=35, blank=True)),
                ('descripcion', models.CharField(max_length=600, blank=True)),
                ('medico', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'gruposdeapoyo',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Leyes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40, blank=True)),
                ('articulo', models.CharField(max_length=15, blank=True)),
                ('descripcion', models.CharField(max_length=300, blank=True)),
            ],
            options={
                'db_table': 'leyes',
                'verbose_name': 'Leyes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Medicamento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40, blank=True)),
                ('presentacion', models.CharField(max_length=30, verbose_name='Presentaci\xf3n', blank=True)),
                ('concentracion', models.CharField(max_length=30, verbose_name='Concentraci\xf3n', blank=True)),
                ('mododeuso', models.CharField(max_length=300, verbose_name='Modo de uso', blank=True)),
                ('contraidicaciones', models.CharField(max_length=500, blank=True)),
                ('descripcion', models.CharField(max_length=500, blank=True)),
            ],
            options={
                'db_table': 'medicamento',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecetaDetalle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dosis', models.CharField(max_length=55, blank=True)),
                ('periodicidad', models.CharField(max_length=20, blank=True)),
                ('fechahorainicio', models.DateTimeField(null=True, blank=True)),
                ('medicamento', models.ForeignKey(to='usuarios.Medicamento')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecetaMedica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fechahora', models.DateTimeField(null=True, blank=True)),
                ('medico', models.ForeignKey(related_name='medico', to=settings.AUTH_USER_MODEL)),
                ('paciente', models.ForeignKey(related_name='paciente', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'recetamedica',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'Servicios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SolicitudesUsuarios',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(unique=True, max_length=20)),
                ('nombre1', models.CharField(max_length=20, verbose_name='Primer Nombre')),
                ('nombre2', models.CharField(max_length=20, verbose_name='Segundo Nombre', blank=True)),
                ('apellido1', models.CharField(max_length=20, verbose_name='Primer Apellido')),
                ('apellido2', models.CharField(max_length=20, verbose_name='Segundo Apellido', blank=True)),
                ('email', models.CharField(max_length=30, db_column='Email')),
                ('telefono', models.CharField(max_length=20, db_column='Telefono', blank=True)),
                ('fechanacimiento', models.DateField(null=True, verbose_name='Fecha de Nacimiento', db_column='fechaNacimiento', blank=True)),
                ('es_paciente', models.BooleanField(default=False, verbose_name='Paciente')),
                ('es_medico_general', models.BooleanField(default=False, verbose_name='M\xe9dico general')),
                ('es_medico_especialista', models.BooleanField(default=False, verbose_name='M\xe9dico especialista')),
            ],
            options={
                'db_table': 'Solicitudes_usuarios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tratamiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40, blank=True)),
                ('duracion', models.IntegerField(null=True, blank=True)),
                ('descripcion', models.CharField(max_length=500, blank=True)),
                ('medico_encargado', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'tratamiento',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='recetadetalle',
            name='receta',
            field=models.ForeignKey(to='usuarios.RecetaMedica'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enfermedad',
            name='tratamiento',
            field=models.ForeignKey(blank=True, to='usuarios.Tratamiento', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datospaciente',
            name='enfermedades',
            field=models.ManyToManyField(to='usuarios.Enfermedad', null=True, db_column='enfermedades', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datospaciente',
            name='eps',
            field=models.ForeignKey(blank=True, to='usuarios.EPS', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datospaciente',
            name='grupos',
            field=models.ForeignKey(db_column='grupos', blank=True, to='usuarios.GruposDeApoyo', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datospaciente',
            name='medicamentos',
            field=models.ForeignKey(db_column='medicamentos', blank=True, to='usuarios.Medicamento', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datospaciente',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datosmedico',
            name='especialidad',
            field=models.ForeignKey(to='usuarios.Especialidades'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datosmedico',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
