# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from usuarios.models.BasicosModel import Medicamento
from usuarios.models.BasicosModel import Enfermedad
from usuarios.models.BasicosModel import GruposDeApoyo
from usuarios.models.BasicosModel import EPS
from django.db import models
from django.conf import settings


class Usuario(AbstractUser):

    es_paciente = models.BooleanField(default=False, blank=True)
    es_medico_general = models.BooleanField(default=False, blank=True)
    es_medico_especialista = models.BooleanField(default=False, blank=True)
    telefono = models.CharField(max_length=20, blank=True)

    objects = UserManager()



    class Meta:
        app_label = 'usuarios'
        permissions = (

                        ("solicitar_tratamiento", "Puede solicitar tratamiento"),
                        ("solicitar_medicamento", "Puede solicitar medicamento"),
                        ("listar_medicos", "Puede listar medicos"),
                        ("listar_pacientes", "Puede listar pacientes"),
                        ("editar_perfil", "Puede editar perfil"),
                        ("editar_perfil_paciente", "Puede editar perfil paciente"),
                        ("listar_grupos_de_apoyo", "Puede listar grupos de apoyo"),
                        ("agregar_a_grupo_apoyo", "Puede agregar a grupo apoyo"),
                        ("listar_servicios", "Puede listar servicios"),
                        ("listar_enfermedades", "Puede listar enfermedades"),
                        ("listar_quejas", "Puede listar quejas"),
                        ("crear_queja", "Puede crear queja"),
                        ("crear_receta", "Puede crear receta"),
                        ("listar_medicamentos", "Puede listar medicamentos"),
                        ("listar_tratamientos", "Puede listar tratamientos"),
                        ("listar_eps", "Puede listar eps"),
                        ("listar_leyes", "Puede listar leyes"),
                        ("asignar_paciente", "Puede asignar paciente"),
                        ("ver_paciente", "Puede ver paciente"),
                    )


class RecetaMedica(models.Model):
    fechahora = models.DateTimeField(blank=True, null=True)
    paciente = models.ForeignKey(Usuario, related_name = 'paciente')
    medico = models.ForeignKey(Usuario, related_name = 'medico')

    def __unicode__(self):
        return 'Receta a '+ self.paciente + ' recetada por ' + self.medico

    class Meta:

        db_table = 'recetamedica'


class RecetaDetalle(models.Model):
    receta = models.ForeignKey(RecetaMedica)
    medicamento = models.ForeignKey(Medicamento)
    dosis = models.CharField(max_length=55, blank=True)
    periodicidad = models.CharField(max_length=20, blank=True)
    fechahorainicio = models.DateTimeField(blank=True, null=True)


class SolicitudesUsuarios(models.Model):
    codigo = models.CharField(max_length=20, unique=True)
    nombre1 = models.CharField(max_length=20, verbose_name='Primer Nombre')
    nombre2 = models.CharField(max_length=20, blank=True, verbose_name='Segundo Nombre')
    apellido1 = models.CharField(max_length=20, verbose_name='Primer Apellido')
    apellido2 = models.CharField(max_length=20, blank=True, verbose_name='Segundo Apellido')
    email = models.CharField(db_column='Email', max_length=30)  # Field name made lowercase.
    estado = models.CharField(max_length=20, choices=settings.ESTADOS_SOLICITUD_USUARIOS, default=settings.ESTADO_ESPERA)
    telefono = models.CharField(db_column='Telefono', max_length=20, blank=True)  # Field name made lowercase.
    fechanacimiento = models.DateField(db_column='fechaNacimiento', blank=True, null=True, verbose_name='Fecha de Nacimiento')
    es_paciente = models.BooleanField(default=False, blank=True, verbose_name='Paciente')
    es_medico_general = models.BooleanField(default=False, blank=True, verbose_name='Médico general')
    es_medico_especialista = models.BooleanField(default=False, blank=True, verbose_name='Médico especialista')


    def __unicode__(self):
        return 'Solicitud de ' + self.nombre1 + ' ' + self.apellido1
    class Meta:
        verbose_name='Solicitudes Usuario'
        db_table = 'Solicitudes_usuarios'


class Especialidades(models.Model):
    nombreespecialidad = models.CharField(max_length=25, blank=True)

    def __unicode__(self):
        return self.nombreespecialidad

    class Meta:
        verbose_name='Especialidade'
        db_table = 'especialidades'


class DatosMedico(models.Model):
    usuario = models.ForeignKey(Usuario)
    foto = models.CharField(max_length=500, blank=True)
    instituciones = models.CharField(max_length=300, blank=True)
    tarifa = models.IntegerField(blank=True, null=True)
    referencias = models.CharField(max_length=500, blank=True)
    especialidad = models.ForeignKey(Especialidades, blank=True, null = True)



    class Meta:

        db_table = 'medicoespecialista'


class DatosPaciente(models.Model):
    usuario = models.ForeignKey(Usuario)
    estrato = models.IntegerField(blank=True, null=True)
    genero = models.CharField(max_length=40, blank=True)
    eps = models.ForeignKey(EPS, blank=True, null=True)
    edad = models.IntegerField(blank=True, null=True)
    direcciondespacho = models.CharField(max_length=40, blank=True)
    nombrecontacto = models.CharField(max_length=45, blank=True)
    peso = models.IntegerField(blank=True, null=True)
    altura = models.IntegerField(blank=True, null=True)
    deportes = models.NullBooleanField()
    enfermedades = models.ManyToManyField(Enfermedad, db_column='enfermedades', blank=True, null=True)
    medicamentos = models.ForeignKey(Medicamento, db_column='medicamentos', blank=True, null=True)
    grupos = models.ForeignKey(GruposDeApoyo, db_column='grupos', blank=True, null=True)

    class Meta:

        db_table = 'paciente'
