# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# from usuarios.models.Usuarios import Usuario
from django.db import models
from django.conf import settings
from django.db.models import Q


class Medicamento(models.Model):
    nombre = models.CharField(max_length=40, blank=True)
    presentacion = models.CharField(max_length=30, blank=True, verbose_name = 'Presentación')
    concentracion = models.CharField(max_length=30, blank=True , verbose_name = 'Concentración')
    mododeuso = models.CharField(max_length=300, blank=True, verbose_name = 'Modo de uso')
    contraidicaciones = models.CharField(max_length=500, blank=True)
    descripcion = models.CharField(max_length=500, blank=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        db_table = 'medicamento'


class Tratamiento(models.Model):
    nombre = models.CharField(max_length=40, blank=True)
    duracion = models.IntegerField(blank=True, null=True, verbose_name = 'Duración')
    medico_encargado = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, verbose_name = 'Medico Encargado')
    descripcion = models.CharField(max_length=500, blank=True, verbose_name = 'Descripción')

    def __unicode__(self):
        return self.nombre

    class Meta:

        db_table = 'tratamiento'



class GruposDeApoyo(models.Model):
    medico = models.ForeignKey(settings.AUTH_USER_MODEL, limit_choices_to = Q(es_medico_general = True) | Q(es_medico_especialista = True),verbose_name = 'Médico' )
    nombre = models.CharField(max_length=35, blank=True)
    descripcion = models.CharField(max_length=600, blank=True,verbose_name = 'Descripción' )

    def __unicode__(self):
        return self.nombre

    class Meta:

        db_table = 'gruposdeapoyo'


class EPS(models.Model):
    nombre = models.CharField(max_length=40, blank=True)
    direccion = models.CharField(max_length=55, blank=True, verbose_name = 'Dirección' )
    telefono = models.CharField(max_length=20, blank=True, verbose_name = 'Teléfono' )

    def __unicode__(self):
        return self.nombre

    class Meta:

        db_table = 'eps'



class Leyes(models.Model):
    nombre = models.CharField(max_length=40, blank=True)
    articulo = models.CharField(max_length=15, blank=True)
    descripcion = models.CharField(max_length=300, blank=True)


    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = "Leye"
        db_table = 'leyes'



class Enfermedad(models.Model):
    nombre = models.CharField(max_length=25, blank=True)
    tratamiento = models.ForeignKey(Tratamiento, blank=True, null=True)
    sintomas = models.CharField(max_length=300, blank=True)
    descripcion = models.CharField(max_length=300, blank=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = "Enfermedade"
        db_table = 'enfermedad'


class Servicio(models.Model):

    class Meta:
        verbose_name = "Servicios"



class SolicitudMedicamento(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300)

    def __unicode__(self):
        return self.nombre
    class Meta:
        db_table = 'solicitud_medicamento'


class SolicitudTratamiento(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300)

    def __unicode__(self):
        return self.nombre
    class Meta:
        db_table = 'solicitud_tratamiento'
