from django.contrib import admin
from models.UsuariosModel import Usuario
from models.UsuariosModel import SolicitudesUsuarios
from models.UsuariosModel import Especialidades
from models.UsuariosModel import DatosMedico
from models.BasicosModel import Medicamento
from models.BasicosModel import Tratamiento
from models.BasicosModel import GruposDeApoyo
from models.BasicosModel import EPS
from models.BasicosModel import Leyes
from models.BasicosModel import Enfermedad
from models.BasicosModel import Servicio

admin.site.register(Usuario)
admin.site.register(SolicitudesUsuarios)
admin.site.register(Especialidades)
admin.site.register(DatosMedico)
admin.site.register(Medicamento)
admin.site.register(Tratamiento)
admin.site.register(GruposDeApoyo)
admin.site.register(EPS)
admin.site.register(Leyes)
admin.site.register(Enfermedad)
admin.site.register(Servicio)




# Register your models here.
