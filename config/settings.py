"""
Django settings for usuarios project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import dj_database_url

from .email_info import EMAIL_USE_TLS,EMAIL_HOST,EMAIL_HOST_USER,EMAIL_HOST_PASSWORD,EMAIL_PORT
#for gmail
EMAIL_USE_TLS = EMAIL_USE_TLS
EMAIL_HOST = EMAIL_HOST
EMAIL_HOST_USER = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = EMAIL_HOST_PASSWORD
EMAIL_PORT = EMAIL_PORT
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
DEFAULT_SECRET_KEY = '3iy-!-d$!pc_ll$#$elg&cpr@*tfn-d5&n9ag=)%#()t$$5%5^'
SECRET_KEY = os.environ.get('SECRET_KEY', DEFAULT_SECRET_KEY)
#SECRET_KEY = '#jz#4di0hc-dulyz3pob4(hptv@n%&q7s-28fox74@19w5@4h0'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True
ADMINS= (
('wilson', 'daniel.p_"407@hotmail.com'),
)

ALLOWED_HOSTS = []



INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'usuarios'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

AUTH_USER_MODEL = 'usuarios.Usuario'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'd2e0vb9t4qifn1',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
            'USER': 'zrwuopunbztkam',
            'PASSWORD': 'zrkU6_LJuZAX8dTxgl42X1oUQR',
            'HOST': 'ec2-54-204-39-187.compute-1.amazonaws.com',                      # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
            'PORT': '5432',                      # Set to empty string for default.
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '../usuarios/static/'


# Parse database configuration from $DATABASE_URL
# DATABASES['default'] =  dj_database_url.config()
# DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'
# DATABASES['default']['PASSWORD'] = 'zrkU6_LJuZAX8dTxgl42X1oUQR'
# DATABASES['default']['NAME'] = 'd2e0vb9t4qifn1'
# DATABASES['default']['USER'] = 'zrwuopunbztkam'
# DATABASES['default']['HOST'] = 'ec2-54-204-39-187.compute-1.amazonaws.com'
# DATABASES['default']['PORT'] = '5432'


# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']
PROJECT_DIR = os.path.dirname(__file__) #for heroku
# Static asset configuration
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'
TEMPLATE_DIRS = (
   os.path.join(PROJECT_DIR, '/templates/'),
    # 'C:/Users/daniel/Desktop/vast-escarpment-5565/usuarios/templates',

)
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),

)


ESTADO_ESPERA = 'espera'
ESTADO_ACEPTADO = 'aceptado'
ESTADO_RECHAZADO = 'rechazado'

ESTADOS_SOLICITUD_USUARIOS = (
    (ESTADO_ESPERA, 'En espera'),
    (ESTADO_ACEPTADO, 'Aceptado'),
    (ESTADO_RECHAZADO, 'Rechazado'),
)
