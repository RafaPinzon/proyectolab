from django.conf.urls import patterns, include, url
# from usuarios.views import Gruposdeapoyo_list,Tratamientos_list,Leyes_list, Medicamento_list,LinkListView, Eps_list,Solicitud_Tratamiento_list,Usuarios_list,Pacientes_list,Solicitudes_medicamentos_list,MedicosG_list,MedicosE_list
from django.contrib import admin


from usuarios.views.UsuariosView import solicitar_usuario, aceptar_solicitud, cancelar_solicitud, eliminar_solicitud, solicitar_tratamiento
from usuarios.views.views import admin_medica, login_usuario, cerrar_sesion, index
from usuarios.views.ListasView import Medicamento_list, Leyes_list, Tratamientos_list, GruposDeApoyo_list, Eps_list
from usuarios.views.ListasView import Pacientes_list, MedicosG_list, MedicosE_list, Medicos_list
from usuarios.views.ListasView import SolicitudesUsuarios_list, SolicitudMedicamento_list, SolicitudTratamiento_list
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin_medica/$', admin_medica, name='admin_medica'),
    url(r'^solicitudes_usuarios/$',SolicitudesUsuarios_list.as_view(), name='solicitudes_usuarios'),
    url(r'^solicitudes_usuarios/aceptar/(?P<solicitud_id>[0-9]{1,9})/$',aceptar_solicitud, name='aceptar_solicitud_usuario'),
    url(r'^solicitudes_usuarios/cancelar/(?P<solicitud_id>[0-9]{1,9})/$',cancelar_solicitud, name='cancelar_solicitud_usuario'),
    url(r'^solicitudes_usuarios/aceptar/(?P<solicitud_id>[0-9]{1,9})/$',aceptar_solicitud, name='aceptar_solicitud_tratamiento'),
    url(r'^solicitudes_usuarios/cancelar/(?P<solicitud_id>[0-9]{1,9})/$',cancelar_solicitud, name='cancelar_solicitud_tratamiento'),
    url(r'^solicitudes_usuarios/eliminar/(?P<solicitud_id>[0-9]{1,9})/$',eliminar_solicitud, name='eliminar_solicitud_usuario'),
    url(r'^solicitudes_medicamentos/$',SolicitudMedicamento_list.as_view(), name='solicitudes_medicamentos'),
    url(r'^solicitudes_tratamientos/$',SolicitudTratamiento_list.as_view(), name='solicitudes_tratamientos'),
    url(r'^pacientes/$',Pacientes_list.as_view(), name='pacientes'),
    url(r'^medicos_generales/$',MedicosG_list.as_view(), name='medicos_generales'),
    url(r'^medicos_especialistas/$',MedicosE_list.as_view(), name='medicos_especialistas'),
    url(r'^medicos/$',Medicos_list.as_view(), name='medicos'),
    url(r'^medicamento/$',Medicamento_list.as_view(), name='medicamento'),
    url(r'^leyes/$',Leyes_list.as_view(), name='leyes'),
    url(r'^tratamientos/$',Tratamientos_list.as_view(), name='tratamientos'),
    url(r'^gruposdeapoyo/$',GruposDeApoyo_list.as_view(), name='gruposdeapoyo'),
    url(r'^eps/$',Eps_list.as_view(), name='eps'),
    url(r'^logout/$', cerrar_sesion, name = 'logout'),
    url(r'^login/$', login_usuario, name = 'login'),
    url(r'^index/$', index, name = 'index'),
    url(r'^$', index, name='index1'),

    url(r'^solicitar_tratamiento/$',solicitar_tratamiento, name='solicitar_tratamiento'),


    # url(r'^medicamentos/$',Medicamento_list.as_view(),name='Medicamento-list'),
    # url(r'^Eps/$',Eps_list.as_view()),
    # url(r'^Leyes/$',Leyes_list.as_view()),
    # url(r'^admin_medica/Tratamientos/$',Tratamientos_list.as_view(),name='tratamientos-list'),
    # url(r'^gruposdeapoyo/$',Gruposdeapoyo_list.as_view()),
    # url(r'^Pacientes/$',Pacientes_list.as_view()),
    # url(r'^usuarios/MedicoE/$',MedicosE_list.as_view()),
    # url(r'^MedicoG/$',MedicosG_list.as_view()),

    # url(r'^index/', usuarios.views.index, name='index'),
    # url(r'^perfil/$', 'usuarios.views.perfil'),

    #url(r'^pagina', lab_software.usuarios.views.index, name= 'index'),

    url(r'^solicitar_usuario/$',solicitar_usuario, name='solicitar_usuario'),
    url(r'^admin/', include(admin.site.urls)),

)
